import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Professional Profile</title>
        <link rel="icon" href="https://quarkscode.com.br/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Edmilson Soares Bezerra
        </h1>

        <p className={styles.title}>js web developer (back / front)</p>
        <p className={styles.title}>@esbnet</p>

        {/* <p className={styles.description}>
          Personal
        </p> */}

        <div className={styles.grid}>
        
          <a
            href="https://www.linkedin.com/in/edmilson-soares/"
            className={styles.card}
          >
            <h3>Linkedin &rarr;</h3>
            <p>
              Career and Personal Development
            </p>
          </a>

          <a
            href="https://www.youtube.com/c/EdmilsonSoares"
            className={styles.card}
          >
            <h3>Youtube &rarr;</h3>
            <p>
              Never stop learning
            </p>
          </a>


          <a href="https://github.com/esbnet/" className={styles.card}>
            <h3>Github &rarr;</h3>
            <p>
              Always open source
            </p>
          </a>

          <a href="https://gitlab.com/esbnet" className={styles.card}>
            <h3>Gitlab &rarr;</h3>
            <p>And more open source</p>
          </a>

        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://quarkscode.com.br/"
          target="_blank"
          rel="noopener noreferrer"
        >
          {/* Powered by{' <quarks_Code>'} */}
          <img src="https://quarkscode.com.br/wp-content/uploads/2020/07/cropped-LogoFundoVazado.png" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
